#### CMOS和BIOS联系：

BIOS中的系统设置程序是完成CMOS参数设置的手段，即通过BIOS设置程序对CMOS参数进行设置。CMOS既是BIOS设置系统参数的存放场所，又是BIOS设置系统参数的结果。

#### CMOS和BIOS联系：

1、本质的不同：

bios保存系统的重要信息和设置系统参数的设置程序，而cmos是主板上的一块可读写的RAM芯片，里面装的是关于系统配置的具体参数，其内容可通过设置程序进行读写。简单来说，BIOS是一段程序，cmos则是芯片。

2、称呼的不同：

对BIOS中各项参数的设定要通过专门的程序。BIOS设置程序一般都被厂商整合在芯片中，在开机时通过特定的按键就可进入BIOS设置程序，方便地对系统进行设置。因此BIOS设置有时也被叫做CMOS设置。

3、存储内容不同：

BIOS中的内容：自诊断程序、CMOS设置程序、系统自举装载程序。BIOS通过读取CMOS的RAM中的内容识别硬件配置，并对其进行自检和初始化。二是CMOS设置程序：引导过程中，用特殊[热键](https://zhidao.baidu.com/search?word=热键&fr=iknow_pc_qb_highlight)启动，进行设置后，存入CMOS RAM中。



计算机的开机四步骤：

1. 开机上电过程
2. 系统自检过程
3. 运行主引导记录过程
4. 装载[操作系统](https://www.zhihu.com/search?q=操作系统&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A924224029})过程
5. 运行操作系统
   - 搜索[计算机硬件设备](https://www.zhihu.com/search?q=计算机硬件设备&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A924224029})，并将硬件设备信息写进操作系统的[注册表文件](https://www.zhihu.com/search?q=注册表文件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A924224029})中。
   - 读入操作系统注册表文件，确定应当加载哪些驱动程序。
   - 装载并执行硬件[设备驱动程序](https://www.zhihu.com/search?q=设备驱动程序&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A924224029})。
   - 创建系统坏境，加载操作系统中的各个核心子系统模块，并执行这些子系统模块。