### BIOS:

如何进入BIOS:

可以按F1、F2、F10、DEL和ESC键，还有些组合键像CTRL + ALT + ESC和CTRL + ALT + DEL。但是这些热键没有工业标准。所以这里建议仔细看启动动画。

##### DELL:

一般是F2是快捷键。

##### HP:

一般是F10，一小部分是F1、F2、F6和F11。

##### 联想：

一般是Enter或者F1。

##### Windows10怎么进入BIOS呢

可以按住Shift键在按重启。

##### 也可以直接用命令开启BIOS:

输入shutdown.exe /r /o在倒计时结束后会进入BIOS界面。



